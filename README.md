# UGE Greed

## Architecture

Le projet est subdivisé en plusieurs parties, le dossier nommé “greed” contient toutes les sources du proje
notamment les readers le client/serveur etc...

À la racine, nous pouvons trouver le dossier doc contenant la RFC que nous avons utilisé, le rapport, ainsi que le dossier
"archives" contenant notre ancienne RFC.

## Manuel utilisateur

Pour démarrer l’application suivre les étapes suivantes :

Vous êtes ROOT :

java --enable-preview -jar greed-1.0-SNAPSHOT.jar <PORT>

Vous vous connectez à un réseau :

java --enable-preview -jar greed-1.0-SNAPSHOT.jar <PORT> <IPPC> <PORTPC>

Avec :
PORT un port sur lequel d’autres applications peuvent se connecter 

PORTPC port de l’application sur lequel vous souhaitez vous connecter 

IPPC ip de l’application sur laquel vous souhaitez vous connecter 
