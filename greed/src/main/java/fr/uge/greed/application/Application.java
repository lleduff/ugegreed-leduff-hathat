package fr.uge.greed.application;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Application {

  private static void usage() {
    System.err.println("Usage : UGEGreed <listening port> <mother ip address> <mother port>");
  }

  public static void main(String[] args) throws IOException {
    if (args.length == 1) {
      try {
        new ClientServer(Integer.parseInt(args[0])).launch();
      } catch (NumberFormatException e) {
        usage();
      }
    } else if (args.length == 3) {
      try {
        new ClientServer(Integer.parseInt(args[0]), new InetSocketAddress(args[1], Integer.parseInt(args[2]))).launch();
      } catch (NumberFormatException e) {
        usage();
      }
    } else {
      usage();
    }
  }
}