package fr.uge.greed.application;

import fr.uge.greed.command.Commande;
import fr.uge.greed.context.Context;
import fr.uge.greed.helpers.Helpers;
import fr.uge.greed.reader.composed.SocketAddress;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientServer {

  private static final Logger logger = Logger.getLogger(ClientServer.class.getName());

  private final SocketChannel socketChannel;

  private final ServerSocketChannel serverSocketChannel;

  private final Selector selector;

  private final InetSocketAddress motherAddress;

  private final InetSocketAddress address;

  private final Thread console;

  public ClientServer(int port) throws IOException {
    this.selector = Selector.open();
    this.serverSocketChannel = ServerSocketChannel.open();
    this.address = new InetSocketAddress(port);
    serverSocketChannel.bind(address);
    this.socketChannel = null;
    this.address = null;
    this.console = new Thread(new Commande(this));
  
  }

  public ClientServer(int port, InetSocketAddress motherAddress) throws IOException {
    this.selector = Selector.open();
    this.serverSocketChannel = ServerSocketChannel.open();
    this.address = new InetSocketAddress(port);
    serverSocketChannel.bind(address);
    this.socketChannel = SocketChannel.open();
    this.address = address;
    this.console = new Thread(new Commande(this));
  }
  public void wakeup(){
    selector.wakeup();
  }

  public void launch() throws IOException {
    serverSocketChannel.configureBlocking(false);
    serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

    console.start();
    if (socketChannel != null) {
      socketChannel.configureBlocking(false);
      var key = socketChannel.register(selector, SelectionKey.OP_CONNECT);
      key.attach(new Context(key, this));
      socketChannel.connect(motherAddress);
    }

    while (!Thread.interrupted()) {
//      Helpers.printKeys(selector);
      try {
        
        selector.select(this::treatKey);
        //fonction;
      } catch (UncheckedIOException tunneled) {
        throw tunneled.getCause();
      } catch (IOException e) {
        logger.info("Application interrupted");
        Thread.currentThread().interrupt();
      }
      for (var entry: routingTable.entrySet()) {
        System.out.println(entry.getKey() + " -> " + entry.getValue());
      }
      System.out.println(" ");
    }
  }

  private void treatKey(SelectionKey selectionKey) {
        try {
          if (selectionKey.isValid() && selectionKey.isAcceptable()) {
            doAccept(selectionKey);
          }
        } catch (IOException e) {
          throw new UncheckedIOException(e);
        }
        try {
          if (selectionKey.isValid() && selectionKey.isConnectable()) {
            ((Context) selectionKey.attachment()).doConnect();
          }
          if (selectionKey.isValid() && selectionKey.isWritable()) {
        ((Context) selectionKey.attachment()).doWrite();
      }
      if (selectionKey.isValid() && selectionKey.isReadable()) {
        ((Context) selectionKey.attachment()).doRead();
      }
    } catch (IOException e) {
      logger.info("Connection closed due to IOException");
      silentlyClose(selectionKey);
    }
  }

  private void doAccept(SelectionKey selectionKey) throws IOException {
    var ssc = (ServerSocketChannel) selectionKey.channel();
    var client = ssc.accept();
    if (client == null) {
      logger.log(Level.SEVERE, "Selector error");
      return;
    }
    logger.info("Connection accepted from " + client.getRemoteAddress());
    client.configureBlocking(false);
    var key = client.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
    key.attach(new Context(key, this));
  }

  private void silentlyClose(SelectionKey k) {
    var sc = k.channel();
    try {
      sc.close();
    } catch (IOException e) {
      // ignore exception
    }
  }

  public InetSocketAddress getMotherAddress() {
    return motherAddress;
  }

  public Map<SocketAddress, Context> getRoutingTable() {
    return routingTable;
  }

  public ServerSocketChannel getServerSocketChannel() {
    return serverSocketChannel;
  }

  public InetSocketAddress getAddress() {
    return address;
  }

  public Selector getSelector() {
    return selector;
  }
}
