package fr.uge.greed.reader.transfert;

import fr.uge.greed.reader.composed.Checker;
import fr.uge.greed.reader.composed.Range;

import java.nio.ByteBuffer;


public record WorkRequest(Checker checker, Range range, Long ub_uc) implements TransfertPayload{
    @Override
    public ByteBuffer toBuffer(){
        var checkerBuffer = checker.toBuffer();
        var rangeBuffer = range.toBuffer();
        int size = Long.BYTES + checkerBuffer.capacity() + rangeBuffer.capacity();
        var buffer = ByteBuffer.allocate(size);
        return buffer.put(checkerBuffer)
        .put(rangeBuffer)
        .putLong(ub_uc)
        .flip();

    }

    
}
