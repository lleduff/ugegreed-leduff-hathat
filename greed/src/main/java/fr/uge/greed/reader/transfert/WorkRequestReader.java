package fr.uge.greed.reader.transfert;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.composed.Checker;
import fr.uge.greed.reader.composed.CheckerReader;
import fr.uge.greed.reader.composed.Range;
import fr.uge.greed.reader.composed.RangeReader;
import fr.uge.greed.reader.primitive.LongReader;

import java.nio.ByteBuffer;

public class WorkRequestReader implements Reader<WorkRequest> {

  private enum State {
    WAITING_CHECKER, WAITING_RANGE, WAITING_UB, ERROR, DONE
  }

  private State state = State.WAITING_CHECKER;

  private final CheckerReader checkerReader = new CheckerReader();

  private final RangeReader rangeReader = new RangeReader();

  private final LongReader longReader = new LongReader();

  private Checker checker;

  private Range range;

  private Long ub_uc;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    switch (state) {
      case WAITING_CHECKER:
        var status = checkerReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        checker = checkerReader.get();
        state = State.WAITING_RANGE;
      case WAITING_RANGE:
        status = rangeReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        range = rangeReader.get();
        state = State.WAITING_UB;
      case WAITING_UB:
        status = longReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        ub_uc = longReader.get();
        state = State.DONE;
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public WorkRequest get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return new WorkRequest(checker, range, ub_uc);
  }

  @Override
  public void reset() {
    state = State.WAITING_CHECKER;
    checkerReader.reset();
    longReader.reset();
    rangeReader.reset();
  }
}