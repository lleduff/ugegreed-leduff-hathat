package fr.uge.greed.reader.composed;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.primitive.LongReader;

import java.nio.ByteBuffer;

public class RangeReader implements Reader<Range> {

  private enum State {
    WAITING_INF, WAITING_SUP, ERROR, DONE
  }

  private State state = State.WAITING_INF;

  private final LongReader longReader = new LongReader();

  private long inf, sup;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    switch (state) {
      case WAITING_INF:
        var status = longReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        inf = longReader.get();
        state = State.WAITING_SUP;
      case WAITING_SUP:
        longReader.reset();
        status = longReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        sup = longReader.get();
        if (inf > sup) {
          state = State.ERROR;
          return ProcessStatus.ERROR;
        }
        state = State.DONE;
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public Range get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return new Range(inf, sup);
  }

  @Override
  public void reset() {
    state = State.WAITING_INF;
    longReader.reset();
  }
}
