package fr.uge.greed.reader.broadcast;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.composed.SocketAddressReader;
import fr.uge.greed.reader.local.Connect;
import fr.uge.greed.reader.local.ConnectReader;

import java.nio.ByteBuffer;

public class AddNodeReader implements Reader<AddNode> {

  private enum State {
    WAITING_ID, ERROR, DONE
  }

  private State state = State.WAITING_ID;

  private final SocketAddressReader socketAddressReader = new SocketAddressReader();

  private AddNode value;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    if (state == State.WAITING_ID) {
      var status = socketAddressReader.process(buffer);
      if (status != ProcessStatus.DONE) {
        return status;
      }
      state = State.DONE;
      value = new AddNode(socketAddressReader.get());
      return ProcessStatus.DONE;
    }
    throw new IllegalStateException();
  }

  @Override
  public AddNode get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return value;
  }

  @Override
  public void reset() {
    state = State.WAITING_ID;
    socketAddressReader.reset();
  }
}
