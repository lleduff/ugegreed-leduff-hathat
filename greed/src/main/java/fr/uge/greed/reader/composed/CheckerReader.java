package fr.uge.greed.reader.composed;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.primitive.StringReader;

import java.nio.ByteBuffer;

public class CheckerReader implements Reader<Checker> {

  private enum State {
    WAITING_URL, WAITING_CLASS, ERROR, DONE
  }

  private State state = State.WAITING_URL;

  private StringReader stringReader = new StringReader();

  private String url, fullyQualifierName;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    switch (state) {
      case WAITING_URL:
        var status = stringReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        url = stringReader.get();
        state = State.WAITING_CLASS;
      case WAITING_CLASS:
        stringReader.reset();
        status = stringReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        fullyQualifierName = stringReader.get();
        state = State.DONE;
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public Checker get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return new Checker(url, fullyQualifierName);
  }

  @Override
  public void reset() {
    state = State.WAITING_URL;
    stringReader.reset();
  }
}
