package fr.uge.greed.reader.local;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.composed.SocketAddressReader;

import java.nio.ByteBuffer;

public class ConnectReader implements Reader<Connect> {

  private enum State {
    WAITING_ID, ERROR, DONE
  }

  private State state = State.WAITING_ID;

  private final SocketAddressReader socketAddressReader = new SocketAddressReader();

  private Connect value;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    if (state == State.WAITING_ID) {
      var status = socketAddressReader.process(buffer);
      if (status != ProcessStatus.DONE) {
        return status;
      }
      state = State.DONE;
      value = new Connect(socketAddressReader.get());
      return ProcessStatus.DONE;
    }
    throw new IllegalStateException();
  }

  @Override
  public Connect get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return value;
  }

  @Override
  public void reset() {
    state = State.WAITING_ID;
    socketAddressReader.reset();
  }
}
