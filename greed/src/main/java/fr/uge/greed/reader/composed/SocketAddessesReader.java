package fr.uge.greed.reader.composed;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.primitive.IntReader;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class SocketAddessesReader implements Reader<List<SocketAddress>> {

  private enum State {
    WAITING_SIZE, WAITING_IDS, ERROR, DONE
  }

  private State state = State.WAITING_SIZE;

  private final IntReader intReader = new IntReader();

  private final SocketAddressReader socketAddressReader = new SocketAddressReader();

  private final List<SocketAddress> value = new ArrayList<>();

  private int size;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    switch (state) {
      case WAITING_SIZE:
        var status = intReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        size = intReader.get();
        if (size < 0) {
          state = State.ERROR;
          return ProcessStatus.ERROR;
        }
        state = State.WAITING_IDS;
      case WAITING_IDS:
        for (var i = 0; i < size; i++) {
          socketAddressReader.reset();
          status = socketAddressReader.process(buffer);
          if (status != ProcessStatus.DONE) {
            return status;
          }
          value.add(socketAddressReader.get());
        }
        if (value.size() != size) {
          state = State.ERROR;
          return ProcessStatus.ERROR;
        }
        state = State.DONE;
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public List<SocketAddress> get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return value;
  }

  @Override
  public void reset() {
    state = State.WAITING_SIZE;
    intReader.reset();
    socketAddressReader.reset();
  }
}
