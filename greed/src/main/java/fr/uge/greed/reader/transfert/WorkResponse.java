package fr.uge.greed.reader.transfert;

import java.nio.ByteBuffer;

public record WorkResponse(Long ub_uc) implements TransfertPayload{
    @Override
    public ByteBuffer toBuffer(){
        int size = Long.BYTES ;
        var buffer = ByteBuffer.allocate(size);
        return buffer.putLong(ub_uc)
        .flip();

    }
    
}
