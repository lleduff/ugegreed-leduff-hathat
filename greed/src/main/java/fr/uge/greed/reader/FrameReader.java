package fr.uge.greed.reader;

import fr.uge.greed.reader.broadcast.BroadcastReader;
import fr.uge.greed.reader.local.LocalReader;
import fr.uge.greed.reader.primitive.ByteReader;
import fr.uge.greed.reader.transfert.TransfertReader;

import java.nio.ByteBuffer;

public class FrameReader implements Reader<Frame> {

  private enum State {
    WAITING_ROUTING, WAITING_PAYLOAD, ERROR, DONE
  }

  private State state = State.WAITING_ROUTING;

  private final ByteReader byteReader = new ByteReader();

  private final LocalReader localReader = new LocalReader();

  private final BroadcastReader broadcastReader = new BroadcastReader();

  private final TransfertReader transfertReader = new TransfertReader();

  private byte opcode;

  private FramePayload payload;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    switch (state) {
      case WAITING_ROUTING:
        var status = byteReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        opcode = byteReader.get();
        state = State.WAITING_PAYLOAD;
      case WAITING_PAYLOAD:
        switch (opcode) {
          case 0 -> {
            status = localReader.process(buffer);
            if (status != ProcessStatus.DONE) {
              return status;
            }
            payload = localReader.get();
          }
          case 1 -> {
            status = transfertReader.process(buffer);
            if (status != ProcessStatus.DONE) {
              return status;
            }
            payload = transfertReader.get();
          }
          case 2 -> {
            status = broadcastReader.process(buffer);
            if (status != ProcessStatus.DONE) {
              return status;
            }
            payload = broadcastReader.get();
          }
          case 3 -> {
            break;    //TODO TO_ROOT
          }
          default -> {
            state = State.ERROR;
            return ProcessStatus.ERROR;
          }
        }
        state = State.DONE;
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public Frame get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return new Frame(opcode, payload);
  }

  @Override
  public void reset() {
    state = State.WAITING_ROUTING;
    broadcastReader.reset();
    localReader.reset();
    transfertReader.reset();
    byteReader.reset();
  }
}
