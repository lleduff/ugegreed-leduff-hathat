package fr.uge.greed.reader;

import java.nio.ByteBuffer;

public interface Reader<T> {

  enum ProcessStatus {
    DONE, //trame trouvée
    REFILL, //il faut plus d'informations, pas assez de données pour trouver une trame entière
    ERROR, //erreur en décodant la trame
  }

  ProcessStatus process(ByteBuffer buffer);

  T get();

  void reset();
}
