package fr.uge.greed.reader.composed;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.primitive.ByteReader;
import fr.uge.greed.reader.primitive.BytesReader;
import fr.uge.greed.reader.primitive.IntReader;

import java.nio.ByteBuffer;

public class SocketAddressReader implements Reader<SocketAddress> {

  private enum State {
    WAITING_FORMAT, WAITING_IP, WAITING_PORT, ERROR, DONE
  }

  private State state = State.WAITING_FORMAT;

  private BytesReader bytesReader;

  private final ByteReader byteReader = new ByteReader();

  private final IntReader intReader = new IntReader();

  private byte type;

  private byte[] ip;

  private int port;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    switch (state) {
      case WAITING_FORMAT:
        var status = byteReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        type = byteReader.get();
        if (type != 4 && type != 6) {
          state = State.ERROR;
          return ProcessStatus.ERROR;
        }
        bytesReader = new BytesReader(type == 4 ? 4 : 16);
        state = State.WAITING_IP;

      case WAITING_IP:
        status = bytesReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        ip = bytesReader.get();
        state = State.WAITING_PORT;
      case WAITING_PORT:
        status = intReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        port = intReader.get();
        if (port < 0 || port > 65535) {
          state = State.ERROR;
          return ProcessStatus.ERROR;
        }
        state = State.DONE;
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public SocketAddress get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return new SocketAddress(type, ip, port);
  }

  @Override
  public void reset() {
    state = State.WAITING_FORMAT;
    if (bytesReader != null) {
      bytesReader.reset();
    }
    byteReader.reset();
    intReader.reset();
  }
}
