package fr.uge.greed.reader.transfert;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.composed.SocketAddress;
import fr.uge.greed.reader.composed.SocketAddressReader;
import fr.uge.greed.reader.primitive.ByteReader;

import java.nio.ByteBuffer;

public class TransfertReader implements Reader<Transfert> {   //TODO TRANSFERT

  private enum State {
    WAITING_OPCODE, WAITING_ID_SRC, WAITING_ID_DST, WAITING_PAYLOAD, ERROR, DONE
  }

  private State state = State.WAITING_OPCODE;

  private final ByteReader byteReader = new ByteReader();

  private final SocketAddressReader socketAddressReader = new SocketAddressReader();

  private byte opcode;

  private SocketAddress id_src, id_dst;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    return null;
  }

  @Override
  public Transfert get() {
    return null;
  }

  @Override
  public void reset() {

  }
}

