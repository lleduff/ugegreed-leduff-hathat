package fr.uge.greed.reader.local;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.primitive.ByteReader;

import java.nio.ByteBuffer;

public class LocalReader implements Reader<Local> {

  private enum State {
    WAITING_OPCODE, WAITING_PAYLOAD, ERROR, DONE
  }

  private State state = State.WAITING_OPCODE;

  private final ByteReader byteReader = new ByteReader();

  private final ConnectOkReader connectOkReader = new ConnectOkReader();

  private final ConnectReader connectReader = new ConnectReader();

  private final ConnectKoReader connectKoReader = new ConnectKoReader();

  private byte opcode;

  private LocalPayload payload;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    switch (state) {
      case WAITING_OPCODE:
        var status = byteReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        opcode = byteReader.get();
        state = State.WAITING_PAYLOAD;
      case WAITING_PAYLOAD:
        switch (opcode) {
          case 1 -> {
            status = connectReader.process(buffer);
            if (status != ProcessStatus.DONE) {
              return status;
            }
            payload = connectReader.get();
          }
          case 2 -> {
            status = connectKoReader.process(buffer);
            if (status != ProcessStatus.DONE) {
              return status;
            }
            payload = connectKoReader.get();
          }
          case 3 -> {
            status = connectOkReader.process(buffer);
            if (status != ProcessStatus.DONE) {
              return status;
            }
            payload = connectOkReader.get();
          }
          default -> {
            state = State.ERROR;
            return ProcessStatus.ERROR;
          }
        }
        state = State.DONE;
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public Local get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return new Local(opcode, payload);
  }

  @Override
  public void reset() {
    state = State.WAITING_OPCODE;
    connectKoReader.reset();
    connectReader.reset();
    connectOkReader.reset();
    byteReader.reset();
  }
}
