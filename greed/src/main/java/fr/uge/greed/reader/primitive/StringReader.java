package fr.uge.greed.reader.primitive;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.utils.Utils;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class StringReader implements Reader<String> {

  private enum State {
    DONE,
    WAITING_INT,
    WAITING_MESSAGE,
    ERROR
  }

  private static final Charset UTF_8 = StandardCharsets.UTF_8;

  private static final int BUFFER_SIZE = 1024;

  private final ByteBuffer internalBuffer = ByteBuffer.allocate(BUFFER_SIZE);

  private State state = State.WAITING_INT;

  private String value;

  private final IntReader intReader = new IntReader();

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    int size = 0;
    switch (state) {
      case WAITING_INT:
        var status = intReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        size = intReader.get();
        if (size < 0 || size > 1024) {
          state = State.ERROR;
          return ProcessStatus.ERROR;
        }
        internalBuffer.limit(size);
        state = State.WAITING_MESSAGE;
      case WAITING_MESSAGE:
        buffer.flip();
        try {
          Utils.put(buffer, internalBuffer);
        } finally {
          buffer.compact();
        }
        if (internalBuffer.hasRemaining()) {
          return ProcessStatus.REFILL;
        }
        state = State.DONE;
        internalBuffer.flip();
        value = UTF_8.decode(internalBuffer).toString();
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public String get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return value;
  }

  @Override
  public void reset() {
    state = State.WAITING_INT;
    intReader.reset();
    internalBuffer.clear();
  }
}
