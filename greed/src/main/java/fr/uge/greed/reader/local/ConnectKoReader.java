package fr.uge.greed.reader.local;

import fr.uge.greed.reader.Reader;

import java.nio.ByteBuffer;

public class ConnectKoReader implements Reader<ConnectKo> {

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    return ProcessStatus.DONE;
  }

  @Override
  public ConnectKo get() {
    return new ConnectKo();
  }

  @Override
  public void reset() {}
}
