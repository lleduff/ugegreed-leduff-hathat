package fr.uge.greed.reader.composed;

import fr.uge.greed.reader.Bufferable;

import java.nio.ByteBuffer;

public record Range(long inf, long sup) implements Bufferable {

  @Override
  public ByteBuffer toBuffer() {
    return ByteBuffer.allocate(Long.BYTES * 2)
        .putLong(inf)
        .putLong(sup)
        .flip();
  }
}
