package fr.uge.greed.reader.primitive;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.utils.Utils;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;

public class BytesReader implements Reader<byte[]> {

    private enum State {
        DONE, WAITING, ERROR
    };

    private State state = State.WAITING;

    private byte[] value;

    private final ByteBuffer internalBuffer;

    public BytesReader(int size) {
        if (size < 0) {
            throw new IllegalArgumentException();
        }
        internalBuffer = ByteBuffer.allocate(size);
    }

    @Override
    public ProcessStatus process(ByteBuffer buffer) {
        if (state == State.DONE || state == State.ERROR) {
            throw new IllegalStateException();
        }
        buffer.flip();
        try {
            Utils.put(buffer, internalBuffer);
        } finally {
            buffer.compact();
        }
        if (internalBuffer.hasRemaining()) {
            return ProcessStatus.REFILL;
        }
        state = State.DONE;
        value = internalBuffer.array();
        return ProcessStatus.DONE;
    }

    @Override
    public byte[] get() {
        if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return value;
    }

    @Override
    public void reset() {
        state = State.WAITING;
        internalBuffer.clear();
    }
}
