package fr.uge.greed.reader.local;

import fr.uge.greed.reader.Bufferable;
import fr.uge.greed.reader.FramePayload;

import java.nio.ByteBuffer;

public record Local(byte opcode, LocalPayload payload) implements FramePayload, Bufferable {

  @Override
  public ByteBuffer toBuffer() {
    var payloadBuffer = payload.toBuffer();
    return ByteBuffer.allocate(Byte.BYTES + payloadBuffer.capacity())
      .put(opcode)
      .put(payloadBuffer)
      .flip();
  }
}