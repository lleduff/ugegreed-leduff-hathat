package fr.uge.greed.reader.transfert;

import fr.uge.greed.reader.Bufferable;
import fr.uge.greed.reader.FramePayload;
import fr.uge.greed.reader.composed.SocketAddress;

import java.nio.ByteBuffer;

public record Transfert(byte opcode, SocketAddress id_src, SocketAddress id_dst, long request_id, TransfertPayload payload) implements FramePayload, Bufferable {

  @Override
  public ByteBuffer toBuffer() {
    var srcBuffer = id_src.toBuffer();
    var dstBuffer = id_dst.toBuffer();
    var payloadBuffer = payload.toBuffer();
    return ByteBuffer.allocate(Byte.BYTES + srcBuffer.capacity() + dstBuffer.capacity() + Long.BYTES + payloadBuffer.capacity())
      .put(opcode)
      .put(dstBuffer)
      .put(srcBuffer)
      .putLong(request_id)
      .put(payloadBuffer)
      .flip();
  }
}