package fr.uge.greed.reader.local;

import fr.uge.greed.reader.Bufferable;

public sealed interface LocalPayload extends Bufferable permits ConnectOk, Connect, ConnectKo {}
