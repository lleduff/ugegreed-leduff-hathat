package fr.uge.greed.reader.transfert;


import fr.uge.greed.reader.composed.Range;
import fr.uge.greed.reader.composed.SocketAddress;

import java.nio.ByteBuffer;
import java.util.List;


public record WorkAssignement(SocketAddress id_dst, SocketAddress id_src, List<Range> ranges) implements TransfertPayload {
  @Override
  public ByteBuffer toBuffer() {
    var bufferIdDst = id_dst.toBuffer();
    var bufferIdSrc = id_src.toBuffer();
    var size = bufferIdSrc.capacity() + bufferIdDst.capacity() + (2 * Long.BYTES * ranges.size());
    var buffer = ByteBuffer.allocate(size);
    for (var range: ranges) {
      buffer.put(range.toBuffer());
    }
    return buffer.put(bufferIdDst)
      .put(bufferIdSrc)
      .flip();
  }
}