package fr.uge.greed.reader.composed;

import fr.uge.greed.reader.Bufferable;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public record Checker(String url, String fullyQualifierName) implements Bufferable {

  @Override
  public ByteBuffer toBuffer(){
    var UTF8 = StandardCharsets.UTF_8;

    var urlBuffer = UTF8.encode(url);
    var nameBuffer = UTF8.encode(fullyQualifierName);
    return ByteBuffer.allocate(urlBuffer.capacity() + nameBuffer.capacity())
        .put(urlBuffer)
        .put(nameBuffer)
        .flip();
  }
}
