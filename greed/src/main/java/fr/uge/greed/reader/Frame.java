package fr.uge.greed.reader;

import java.nio.ByteBuffer;

public record Frame(byte routing, FramePayload payload) implements Bufferable {

  @Override
  public ByteBuffer toBuffer() {
    var payloadBuffer = payload.toBuffer();
    return ByteBuffer.allocate(Byte.BYTES + payloadBuffer.capacity())
      .put(routing)
      .put(payloadBuffer)
      .flip();
  }
}
