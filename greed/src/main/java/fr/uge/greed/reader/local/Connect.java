package fr.uge.greed.reader.local;

import fr.uge.greed.reader.composed.SocketAddress;

import java.nio.ByteBuffer;

public record Connect(SocketAddress id) implements LocalPayload {

  @Override
  public ByteBuffer toBuffer() {
    var idBuffer = id.toBuffer();
    return ByteBuffer.allocate(idBuffer.capacity())
      .put(idBuffer)
      .flip();
  }
}
