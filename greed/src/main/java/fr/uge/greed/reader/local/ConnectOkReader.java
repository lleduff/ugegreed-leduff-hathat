package fr.uge.greed.reader.local;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.composed.SocketAddessesReader;
import fr.uge.greed.reader.composed.SocketAddress;
import fr.uge.greed.reader.composed.SocketAddressReader;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class ConnectOkReader implements Reader<ConnectOk> {

  private enum State {
    WAITING_ID, WAITING_IDS, ERROR, DONE
  }

  private State state = State.WAITING_ID;

  private final SocketAddressReader socketAddressReader = new SocketAddressReader();

  private final SocketAddessesReader socketAddessesReader = new SocketAddessesReader();

  private SocketAddress id;

  private List<SocketAddress> ids;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    switch (state) {
      case WAITING_ID:
        var status = socketAddressReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        id = socketAddressReader.get();
        state = State.WAITING_IDS;
      case WAITING_IDS:
        status = socketAddessesReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        state = State.DONE;
        ids = new ArrayList<>(socketAddessesReader.get());
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public ConnectOk get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return new ConnectOk(id, ids);
  }

  @Override
  public void reset() {
    state = State.WAITING_ID;
    socketAddessesReader.reset();
    socketAddressReader.reset();
  }
}
