package fr.uge.greed.reader.local;

import java.nio.ByteBuffer;

public record ConnectKo() implements LocalPayload {

  @Override
  public ByteBuffer toBuffer() {
    return ByteBuffer.allocate(0)
      .flip();
  }
}
