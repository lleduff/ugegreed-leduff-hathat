package fr.uge.greed.reader.local;

import fr.uge.greed.reader.composed.SocketAddress;

import java.nio.ByteBuffer;
import java.util.List;

public record ConnectOk(SocketAddress id, List<SocketAddress> listIds) implements LocalPayload {

  @Override
  public ByteBuffer toBuffer() {
    var idBuffer = id.toBuffer();
    var size = idBuffer.capacity() + Integer.BYTES;
    for (var idx: listIds) {
      size += idx.toBuffer().capacity();
    }
    var buffer = ByteBuffer.allocate(size)
      .put(idBuffer)
      .putInt(listIds.size());
    for (var idx: listIds) {
      buffer.put(idx.toBuffer());
    }
    return buffer.flip();
  }
}