package fr.uge.greed.reader.transfert;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.primitive.LongReader;

import java.nio.ByteBuffer;

public class WorkResponseReader implements Reader<WorkResponse> {

  private enum State {
    WAITING_UB, ERROR, DONE
  }

  private State state = State.WAITING_UB;

  private final LongReader LongReader = new LongReader();

  private Long ub_uc;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    switch (state) {
      case WAITING_UB:
        var status = LongReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        ub_uc = LongReader.get();
        state = State.DONE;
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public WorkResponse get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return new WorkResponse(ub_uc);
  }

  @Override
  public void reset() {
    state = State.WAITING_UB;
    LongReader.reset();
  }
}
