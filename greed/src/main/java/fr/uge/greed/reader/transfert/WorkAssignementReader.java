package fr.uge.greed.reader.transfert;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.composed.Range;
import fr.uge.greed.reader.composed.RangesReader;
import fr.uge.greed.reader.composed.SocketAddress;
import fr.uge.greed.reader.composed.SocketAddressReader;

import java.nio.ByteBuffer;
import java.util.List;


public class WorkAssignementReader implements Reader<WorkAssignement> {

  private enum State {
    WAITING_IDS, WAITING_IDD, WAITING_RANGES, ERROR, DONE
  }

  private State state = State.WAITING_IDS;

  private final SocketAddressReader idReader = new SocketAddressReader();

  private final RangesReader ragesReader = new RangesReader();

  
  private SocketAddress id_dst; 

  private SocketAddress id_src;

  private List<Range> ranges;
  
  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    switch (state) {
      case WAITING_IDS:
        var status = idReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        id_dst = idReader.get();
        state = State.WAITING_IDD;
        idReader.reset();
      case WAITING_IDD:
        status = idReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        id_src = idReader.get();
        state = State.WAITING_RANGES;
      case WAITING_RANGES:
        status = ragesReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        ranges = ragesReader.get();
        state = State.DONE;
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public WorkAssignement get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return new WorkAssignement(id_dst, id_src, ranges);
  }

  @Override
  public void reset() {
    state = State.WAITING_IDS;
    idReader.reset();
    ragesReader.reset();
  }
}