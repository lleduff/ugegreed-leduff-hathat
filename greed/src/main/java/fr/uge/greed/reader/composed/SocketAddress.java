package fr.uge.greed.reader.composed;

import fr.uge.greed.reader.Bufferable;

import java.nio.ByteBuffer;
import java.util.Arrays;

public record SocketAddress(byte format, byte[] ip, int port) implements Bufferable {

  @Override
  public ByteBuffer toBuffer() {
    return ByteBuffer.allocate(Byte.BYTES + ip.length + Integer.BYTES)
      .put(format)
      .put(ip)
      .putInt(port)
      .flip();
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof SocketAddress address
        && format == address.format
        && Arrays.equals(ip, address.ip)
        && port == address.port;
  }

  @Override
  public int hashCode() {
    return Byte.hashCode(format) ^ Arrays.hashCode(ip) ^ Integer.hashCode(port);
  }
}