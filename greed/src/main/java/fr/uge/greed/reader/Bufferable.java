package fr.uge.greed.reader;

import java.nio.ByteBuffer;

public interface Bufferable {

  /**
   * Renvoie la trame dans un ByteBuffer en mode lecture
   */
  ByteBuffer toBuffer();
}
