package fr.uge.greed.reader.broadcast;

import fr.uge.greed.reader.Bufferable;
import fr.uge.greed.reader.FramePayload;
import fr.uge.greed.reader.broadcast.BroadcastPayload;
import fr.uge.greed.reader.composed.SocketAddress;

import java.nio.ByteBuffer;

public record Broadcast(byte opcode, SocketAddress id_src, BroadcastPayload payload) implements FramePayload, Bufferable {

  @Override
  public ByteBuffer toBuffer() {
    var srcBuffer = id_src.toBuffer();
    var payloadBuffer = payload.toBuffer();
    return ByteBuffer.allocate(Byte.BYTES + srcBuffer.capacity() + payloadBuffer.capacity())
      .put(opcode)
      .put(srcBuffer)
      .put(payloadBuffer)
      .flip();
  }
}