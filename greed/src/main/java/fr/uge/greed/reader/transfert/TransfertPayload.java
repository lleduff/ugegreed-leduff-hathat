package fr.uge.greed.reader.transfert;

import fr.uge.greed.reader.Bufferable;

public sealed interface TransfertPayload extends Bufferable permits WorkAssignement, WorkRequest, WorkResponse {}
