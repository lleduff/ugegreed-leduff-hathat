package fr.uge.greed.reader.composed;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.primitive.IntReader;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class RangesReader implements Reader<List<Range>> {

  private enum State {
    WAITING_SIZE, WAITING_RANGES, ERROR, DONE
  }

  private State state = State.WAITING_SIZE;

  private final IntReader intReader = new IntReader();

  private final RangeReader rangeReader = new RangeReader();

  private final List<Range> value = new ArrayList<>();

  private int size;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    switch (state) {
      case WAITING_SIZE:
        var status = intReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        size = intReader.get();
        if (size < 0) {
          state = State.ERROR;
          return ProcessStatus.ERROR;
        }
        state = State.WAITING_RANGES;
      case WAITING_RANGES:
        for (var i = 0; i < size; i++) {
          rangeReader.reset();
          status = rangeReader.process(buffer);
          if (status != ProcessStatus.DONE) {
            return status;
          }
          value.add(rangeReader.get());
        }
        if (value.size() != size) {
          state = State.ERROR;
          return ProcessStatus.ERROR;
        }
        state = State.DONE;
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public List<Range> get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return value;
  }

  @Override
  public void reset() {
    state = State.WAITING_SIZE;
    intReader.reset();
    rangeReader.reset();
  }
}
