package fr.uge.greed.reader.broadcast;

import fr.uge.greed.reader.Bufferable;

public sealed interface BroadcastPayload extends Bufferable permits AddNode {}
