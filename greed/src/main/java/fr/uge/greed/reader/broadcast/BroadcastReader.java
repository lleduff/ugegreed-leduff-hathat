package fr.uge.greed.reader.broadcast;

import fr.uge.greed.reader.Reader;
import fr.uge.greed.reader.composed.SocketAddress;
import fr.uge.greed.reader.composed.SocketAddressReader;
import fr.uge.greed.reader.primitive.ByteReader;

import java.nio.ByteBuffer;

public class BroadcastReader implements Reader<Broadcast> {

  private enum State {
    WAITING_OPCODE, WAITING_ID_SRC, WAITING_PAYLOAD, ERROR, DONE
  }

  private State state = State.WAITING_OPCODE;

  private final ByteReader byteReader = new ByteReader();

  private final SocketAddressReader socketAddressReader = new SocketAddressReader();

  private final AddNodeReader addNodeReader = new AddNodeReader();

  private byte opcode;

  private SocketAddress id_src;

  private BroadcastPayload payload;

  @Override
  public ProcessStatus process(ByteBuffer buffer) {
    switch (state) {
      case WAITING_OPCODE:
        var status = byteReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        opcode = byteReader.get();
        state = State.WAITING_PAYLOAD;
      case WAITING_ID_SRC:
        status = socketAddressReader.process(buffer);
        if (status != ProcessStatus.DONE) {
          return status;
        }
        id_src = socketAddressReader.get();
        state = State.WAITING_PAYLOAD;
      case WAITING_PAYLOAD:
        if (opcode == 4) {
          status = addNodeReader.process(buffer);
          if (status != ProcessStatus.DONE) {
            return status;
          }
          payload = addNodeReader.get();
        } else {
          state = State.ERROR;
          return ProcessStatus.ERROR;
        }
        state = State.DONE;
        return ProcessStatus.DONE;
      default:
        throw new IllegalStateException();
    }
  }

  @Override
  public Broadcast get() {
    if (state != State.DONE) {
      throw new IllegalStateException();
    }
    return new Broadcast(opcode, id_src, payload);
  }

  @Override
  public void reset() {
    state = State.WAITING_OPCODE;
    byteReader.reset();
    socketAddressReader.reset();
    addNodeReader.reset();
  }
}
