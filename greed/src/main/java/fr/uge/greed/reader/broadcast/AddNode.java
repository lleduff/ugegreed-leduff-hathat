package fr.uge.greed.reader.broadcast;

import fr.uge.greed.reader.composed.SocketAddress;

import java.nio.ByteBuffer;

public record AddNode(SocketAddress id) implements BroadcastPayload {

  @Override
  public ByteBuffer toBuffer() {
    var idBuffer = id.toBuffer();
    return ByteBuffer.allocate(idBuffer.capacity())
      .put(idBuffer)
      .flip();
  }
}