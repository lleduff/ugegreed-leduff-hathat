package fr.uge.greed.command;


import java.util.logging.*;

import fr.uge.greed.application.ClientServer;

import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;

public class Commande implements Runnable{
    private final ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<>(10);
    private static final Logger logger = Logger.getLogger(Commande.class.getName());
    private final ClientServer client;

    public Commande(ClientServer client) {
        this.client = Objects.requireNonNull(client);
    }

    @Override
    public void run() {

        try {
            try (var scanner = new Scanner(System.in)) {
                while (scanner.hasNextLine()) {
                    var command = scanner.nextLine();
                    sendCommand(command);
                }
            }
            logger.info("Console thread stopping");
        } catch (InterruptedException e) {
            logger.info("Console thread has been interrupted");
        }
    }

    private void sendCommand(String msg) throws InterruptedException {
        queue.put(msg);
        System.out.println(msg);
        System.out.println("aaaa");
        client.wakeup();
        
      }
    void processCommands(){
        
    }


}
