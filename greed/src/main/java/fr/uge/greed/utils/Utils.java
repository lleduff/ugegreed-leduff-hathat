package fr.uge.greed.utils;

import fr.uge.greed.reader.composed.SocketAddress;

import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Objects;

public class Utils {

  public static void put(ByteBuffer src, ByteBuffer dst) {
    Objects.requireNonNull(src);
    Objects.requireNonNull(dst);
    if (src.remaining() <= dst.remaining()) {
      dst.put(src);
    } else {
      var oldLimit = src.limit();
      src.limit(dst.remaining());
      dst.put(src);
      src.limit(oldLimit);
    }
  }

  public static SocketAddress inetToFrame(InetSocketAddress inet) {
    var format = inet.getAddress() instanceof Inet4Address ? (byte) 4 : (byte) 8;
    return new SocketAddress(format, inet.getAddress().getAddress(), inet.getPort());
  }
}
