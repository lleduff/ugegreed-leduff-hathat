package fr.uge.greed.context;

import fr.uge.greed.application.ClientServer;
import fr.uge.greed.reader.Frame;
import fr.uge.greed.reader.FrameReader;
import fr.uge.greed.reader.broadcast.AddNode;
import fr.uge.greed.reader.broadcast.Broadcast;
import fr.uge.greed.reader.local.Connect;
import fr.uge.greed.reader.local.ConnectKo;
import fr.uge.greed.reader.local.ConnectOk;
import fr.uge.greed.reader.local.Local;
import fr.uge.greed.reader.transfert.Transfert;
import fr.uge.greed.reader.transfert.WorkAssignement;
import fr.uge.greed.reader.transfert.WorkRequest;
import fr.uge.greed.reader.transfert.WorkResponse;
import fr.uge.greed.utils.Utils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.Channel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Context {

  private static final Logger logger = Logger.getLogger(Context.class.getName());

  private static final int BUFFER_SIZE = 10000;

  private final SelectionKey selectionKey;

  private final SocketChannel socketChannel;

  private final ByteBuffer bufferIn = ByteBuffer.allocate(BUFFER_SIZE);

  private final ByteBuffer bufferOut = ByteBuffer.allocate(BUFFER_SIZE);

  private boolean closed = false;

  private final ArrayDeque<Frame> queue = new ArrayDeque<>();

  private final FrameReader reader = new FrameReader();

  private final ClientServer server;


  public Context(SelectionKey selectionKey, ClientServer server) {
    this.selectionKey = selectionKey;
    this.socketChannel = (SocketChannel) selectionKey.channel();
    this.server = server;
  }

  private void updateInterestOps() {
    var interestOps = 0;
    if (!closed && bufferIn.hasRemaining()) {
      interestOps |= SelectionKey.OP_READ;
    }
    if (bufferOut.position() != 0) {
      interestOps |= SelectionKey.OP_WRITE;
    }
    try {
      selectionKey.interestOps(interestOps);
    } catch (CancelledKeyException e) { /* ignore */ }
  }

  public void doRead() throws IOException {
    var read = socketChannel.read(bufferIn);
    if (read == -1) {
      logger.log(Level.SEVERE, "Connexion closed from " + socketChannel.getRemoteAddress());
      closed = true;
    }
    if (read == 0) {
      logger.log(Level.SEVERE, "Selector error");
    }
    processIn();
    updateInterestOps();
  }

  public void doWrite() throws IOException {
    bufferOut.flip();
    socketChannel.write(bufferOut);
    bufferOut.compact();
    processOut();
    updateInterestOps();
  }

  public void doConnect() throws IOException {
    if (!socketChannel.finishConnect()) {
      return;
    }
    var motherAddress = server.getMotherAddress();
    if (motherAddress != null) {
      add(new Frame((byte) 0, new Local((byte) 1, new Connect(Utils.inetToFrame(server.getAddress())))));
    }

  }

  private void processOut() {
    while (!queue.isEmpty()) {
      var frame = queue.poll();
      bufferOut.put(frame.routing());
      bufferOut.put(frame.payload().toBuffer());
    }
  }

  private void processIn() throws IOException {
    var status = reader.process(bufferIn);
    switch (status) {
      case REFILL -> {
        logger.log(Level.SEVERE, "REFILL");
      }
      case ERROR -> {
        logger.log(Level.SEVERE, "ERROR");
        silentlyClose();
      }
      case DONE -> {
        var frame = reader.get();
        reader.reset();
        treatFrame(frame);
      }
    }
  }

  public void add(Frame frame) {
    queue.add(frame);
    processOut();
    updateInterestOps();
  }


  private void treatFrame(Frame frame) throws IOException {
    switch (frame.routing()) {
      case 0 -> {
        switch (((Local) frame.payload()).payload()) {
          case Connect connect -> {
            add(new Frame((byte) 0, new Local((byte) 3, new ConnectOk(Utils.inetToFrame(server.getAddress()), new ArrayList<>(server.getRoutingTable().keySet())))));
            server.getRoutingTable().put(connect.id(), this);
            for (var key: server.getSelector().keys()) {
              var context = (Context) key.attachment();
              if (context != null && context != this) {
                context.add(new Frame((byte) 2, new Broadcast((byte) 4, Utils.inetToFrame(server.getAddress()), new AddNode(connect.id()))));
              }
            }
          }
          case ConnectKo ignored -> {
            silentlyClose();
          }
          case ConnectOk ok -> {
            for (var key: server.getSelector().keys()) {
              var motherContext = (Context) key.attachment();
              if (motherContext != null) {
                var sc = (Channel) key.channel();
                if (sc.equals(motherContext.socketChannel)) {
                  server.getRoutingTable().put(ok.id(), motherContext);
                  for (var idx: ok.listIds()) {
                    server.getRoutingTable().put(idx, motherContext);
                  }
                }
              }
            }
          }
        }
      }
      case 1 -> {
        var transfert = ((Transfert) frame.payload());
        switch (transfert.payload()) {
          case WorkRequest workRequest -> {
            add(new Frame((byte) 1, new Transfert((byte) 5, transfert.id_dst(), transfert.id_dst(), transfert.request_id(), new WorkResponse(workRequest.ub_uc()))));
          }
          case WorkResponse workResponse -> {}
          case WorkAssignement workAssignement -> {}
        }
      }
      case 2 -> {
        var broadcast = ((Broadcast) frame.payload());
        switch (broadcast.payload()) {
          case AddNode addNode -> {
            for (var key: server.getSelector().keys()) {
              var context = (Context) key.attachment();
              if (context != null) {
                server.getRoutingTable().put(addNode.id(), context.server.getRoutingTable().get(broadcast.id_src()));
  //                if (!context.socketChannel.equals((Channel) key.channel())) {
  //                  context.add(new Frame((byte) 2, new Broadcast((byte) 4, Utils.inetToFrame(server.getAddress()), addNode)));
  //                }
              }
            }
          }
        }
      }
      case 3 -> {}
      case 4 -> {}
      default -> {}
    }
  }

  private void silentlyClose() {
    try {
      socketChannel.close();
    } catch (IOException e) {
      // ignore exception
    }
  }

  @Override
  public String toString() {
    try {
      return "Context[" + socketChannel.getRemoteAddress() + "]";
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
